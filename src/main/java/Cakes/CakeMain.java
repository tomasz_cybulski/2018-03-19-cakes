package Cakes;

import java.io.*;
import java.lang.reflect.Array;

/**
 * Hello world!
 *
 */
public class CakeMain
{



    public static void main( String[] args )
    {

        Cake[] cakes = new Cake[5];
        File file = new File("/tmp/cakes");

        cakes[0] = new WeddingCake(30, 100, 5, 5);
        cakes[1] = new WeddingCake(60, 150, 10, 8);
        cakes[2] = new WeddingCake(90, 200, 15, 12);
        cakes[3] = new BirthdayCake(10, 10, 1, 18);
        cakes[4] = new BirthdayCake(15, 10, 2, 30);

        bake(cakes);


        int birthdayCakesSum = countBirthdayCakes(cakes);
        System.out.println("We have "+birthdayCakesSum+" birthdayCakes.");

        save(cakes,file);


    }

    static void bake(Cake[] cakes){
        System.out.println("Cakes baking:");
        for(Cake i : cakes){
            if(i instanceof WeddingCake){
                i.bake();
            }
            if(i instanceof BirthdayCake){
                i.bake();
            }
        }
    }

    static int countBirthdayCakes(Cake[] cakes){
        int sum = 0;
        for(Cake i : cakes){
            if(i instanceof BirthdayCake){
                sum++;
            }
        }
        return sum;
    }



    static void save(Cake[] cakes, File file){
        ObjectOutputStream out = null;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {            oos.writeObject(cakes);        } catch (IOException ex) {
            System.err.println(ex);
        }

    }
}
