package Cakes;

import java.io.Serializable;

public class WeddingCake extends Cake implements Serializable{

    private int tiresNumber;

    public WeddingCake(){}

    public WeddingCake (int diameter, int height, int weight, int tiresNumber){
    super(diameter, height, weight);
    this.tiresNumber = tiresNumber;
    }

    public int getTiresNumber() {
        return tiresNumber;
    }

    public void setTiresNumber(int tiresNumber) {
        this.tiresNumber = tiresNumber;
    }

    @Override
    public void bake(){
        System.out.println("You're baking weddingCake");
    }
}
