package Cakes;

import java.io.Serializable;

public class BirthdayCake extends Cake implements Serializable {


    private int candlesNumber;

    public BirthdayCake(){}

    public BirthdayCake (int diameter, int height, int weight, int candlesNumber){
        super(diameter, height, weight);
        this.candlesNumber = candlesNumber;
    }

    public int getCandlesNumber() {
        return candlesNumber;
    }

    public void setCandlesNumber(int candlesNumber) {
        this.candlesNumber = candlesNumber;
    }

    @Override
    public void bake(){
        System.out.println("You're baking birthdayCake");
    }
}

