package Cakes;

public abstract class Cake{

    private int diameter;
    private int height;
    private int weight;

    public Cake(){}

    public Cake(int diameter, int height, int weight){
        this.diameter = diameter;
        this.height = height;
        this.weight = weight;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public abstract void bake();

    public void print(){
        System.out.println("Diameter: "+diameter+" Height: "+height+" Weight: "+weight);
    }
}
